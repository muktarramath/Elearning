﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Elearning.Models
{
    
    public class tempEtudiant
    {
        [Required(ErrorMessage = "Prenom is required.")]
        public string Prenom { get; set; }

        [Required(ErrorMessage = "Nom is required.")]
        public string Nom { get; set; }

        [Required(ErrorMessage = "Email is required.")]
        [EmailAddress(ErrorMessage = "Invalid Email Address.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Telephone is required.")]
        public string Telephone { get; set; }
    }
}