﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Elearning.Models;

namespace Elearning.Controllers
{
    public class GestionnaireController : Controller
    {
       DataElearningClassesDataContext EL = new DataElearningClassesDataContext();
        // GET: Gestionnaire
        public ActionResult Index()
        {

            var gestionStud = TablEtudiants();


            return View(gestionStud);
        }

        [HttpPost]
        public ActionResult Index( int? idetudiant)
        {
         
            if (Request.Form["edit"] != null && idetudiant != null)
            {

                var etudiant = (from p in EL.Etudiants
                    where p.Idetudiant.Equals(idetudiant)
                    select p).SingleOrDefault();



                ViewBag.student = etudiant;
               
                return View();

            }
            else if ((Request.Form["annuler"] != null) || idetudiant == null)
            {
                Index();
            }

            return View();
        }

        public IQueryable<Etudiant>  TablEtudiants()
        {
            var gestionStud = from p in EL.Etudiants
                select p;
            return gestionStud;
        }



    }
}