﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Web.DynamicData;
using System.Web.Mvc;
using Elearning.Models;
using Microsoft.Ajax.Utilities;

namespace Elearning.Controllers
{
    public class HomeController : Controller
    {
        DataElearningClassesDataContext EL = new DataElearningClassesDataContext();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string uname, string pwd,string chkbox,string other)
        {
            if (chkbox == null)
            {
                Etudiant student = EL.Etudiants.FirstOrDefault(w => w.uname.Equals(uname) && w.pwd.Equals(pwd));
                if (student == null)
                {
                    ViewBag.Message = "Vous n'etes pas enregistrer ou information erronnees.";
                    return View("Error");
                }
                else if (student.webregister.Equals(1))
                {
                    return RedirectToAction("Index", "Student", new { area = "" });
                }
                else
                {
                    return RegisterConfirm();
                }
               


            }
            else
            {
                if (other.Equals("Enseignant"))
                {

                    Enseignant teacher = EL.Enseignants.FirstOrDefault(w => w.uname.Equals(uname) && w.pwd.Equals(pwd));
                    if (teacher == null)
                    {
                        ViewBag.Message = "Vous n'etes pas enregistrer ou information erronnees.";
                        return View("Error");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Teacher", new { area = "" });
                    }


                    
                }
                else if (other.Equals("Gestionnaire"))
                {
                    Gestionnaire gest = EL.Gestionnaires.FirstOrDefault(w => w.uname.Equals(uname) && w.pwd.Equals(pwd));
                    if (gest == null)
                    {
                        ViewBag.Message = "Vous n'etes pas enregistrer ou information erronnees.";
                        return View("Error");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Gestionnaire", new { area = "" });
                    }
                   
                }
                else
                {
                    return View();
                }
            }
            
        }
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(tempEtudiant temp)
        {
            Random r = new Random();
           
            string pwdgenerated =temp.Telephone.Substring(0  , 3) + (r.Next(7777, 9999)).ToString()+ temp.Prenom.Substring(0, 1);
            
           Etudiant tmp = new Etudiant{prenom = temp.Prenom, nom = temp.Nom,email = temp.Email, uname = temp.Prenom.Substring(0,2)+temp.Telephone.Substring(4 , 2)+temp.Nom.Substring(0,2),pwd= pwdgenerated, telephone = temp.Telephone};
            EL.Etudiants.InsertOnSubmit(tmp);
            EL.SubmitChanges();

            ViewBag.pwd = tmp.pwd;
            ViewBag.email = tmp.email;
            return View("RegisterConfirm");
        }

        public ActionResult RegisterConfirm()
        {
            return View();
        }

        public ActionResult Error(Exception e)
        {
            return View(e.Message);
        }

        
    }
}